import * as h from 'ts-gib/dist/helper.mjs';
import * as core from 'core-gib';

export function foo(): void {
    console.log('fooo yo again hmm 2:48PM...');
}

foo();

let id = await h.getUUID();
console.log(id);

foo();

let lc = '[plain-gib]';
let ibgib = await core.fooFactory();
console.log(`${lc} ibgib from fooFactory: ${h.pretty(ibgib)}`);

let count = 0;
let interval = setInterval(async () => {
    ibgib = await core.fooFactory();
    console.log(`${lc} ibgib now is from fooFactory: ${h.pretty(ibgib)}`);
    count++;
    if (count > 5) { clearInterval(interval); console.log(`that's enough of that`); }
}, 1010);
