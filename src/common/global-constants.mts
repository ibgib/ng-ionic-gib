
/**
 * Used as a flag for front-end debug borders.
 */
export const GLOBAL_DEBUG_BORDER = false;